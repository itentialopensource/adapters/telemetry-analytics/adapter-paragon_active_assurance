## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Paragon Active Assurance. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Paragon Active Assurance.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Paragon_active_assurance. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">listAlarmEmails(account, limit, offset, callback)</td>
    <td style="padding:15px">list_alarm_emails</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarm_emails/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAlarmEmails(account, body, callback)</td>
    <td style="padding:15px">create_alarm_emails</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarm_emails/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAlarmEmails(account, alarmEmailsId, callback)</td>
    <td style="padding:15px">delete_alarm_emails</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarm_emails/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmEmails(account, alarmEmailsId, callback)</td>
    <td style="padding:15px">get_alarm_emails</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarm_emails/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlarmEmails(account, alarmEmailsId, body, callback)</td>
    <td style="padding:15px">update_alarm_emails</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarm_emails/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAlarmTemplates(account, limit, offset, callback)</td>
    <td style="padding:15px">list_alarm_templates</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarm_templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAlarmTemplates(account, body, callback)</td>
    <td style="padding:15px">create_alarm_templates</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarm_templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAlarmTemplate(account, alarmTemplateId, callback)</td>
    <td style="padding:15px">delete_alarm_template</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarm_templates/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmTemplate(account, alarmTemplateId, callback)</td>
    <td style="padding:15px">get_alarm_template</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarm_templates/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlarmTemplate(account, alarmTemplateId, body, callback)</td>
    <td style="padding:15px">update_alarm_template</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarm_templates/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAlarms(account, limit, offset, createdTimespanStart, createdTimespanEnd, clearedTimespanStart, clearedTimespanEnd, suppressed, isSuppressed, isActive, isCleared, interfaceName, testAgent, task, callback)</td>
    <td style="padding:15px">list_alarms</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarms/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAlarm(account, alarmId, callback)</td>
    <td style="padding:15px">delete_alarm</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarms/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarm(account, alarmId, callback)</td>
    <td style="padding:15px">get_alarm</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarms/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlarm(account, alarmId, body, callback)</td>
    <td style="padding:15px">update_alarm</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/alarms/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listExternalUrlShares(account, limit, offset, callback)</td>
    <td style="padding:15px">list_external_url_shares</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/external_url_shares/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createExternalUrlShare(account, body, callback)</td>
    <td style="padding:15px">create_external_url_share</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/external_url_shares/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExternalUrlShare(account, shareId, callback)</td>
    <td style="padding:15px">delete_external_url_share</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/external_url_shares/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalUrlShare(account, shareId, callback)</td>
    <td style="padding:15px">get_external_url_share</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/external_url_shares/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateExternalUrlShare(account, shareId, body, callback)</td>
    <td style="padding:15px">update_external_url_share</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/external_url_shares/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listIpLookupTable(account, limit, offset, tag, tagName, callback)</td>
    <td style="padding:15px">list_ip_lookup_table</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/ip_lookup_table/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIpLookupTable(account, body, callback)</td>
    <td style="padding:15px">create_ip_lookup_table</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/ip_lookup_table/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpLookupTable(account, ipLookupTableId, callback)</td>
    <td style="padding:15px">delete_ip_lookup_table</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/ip_lookup_table/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpLookupTable(account, ipLookupTableId, callback)</td>
    <td style="padding:15px">get_ip_lookup_table</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/ip_lookup_table/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIpLookupTable(account, ipLookupTableId, body, callback)</td>
    <td style="padding:15px">update_ip_lookup_table</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/ip_lookup_table/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listIptvChannels(account, limit, offset, tag, tagName, callback)</td>
    <td style="padding:15px">list_iptv_channels</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/iptv_channels/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIptvChannel(account, body, callback)</td>
    <td style="padding:15px">create_iptv_channel</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/iptv_channels/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIptvChannel(account, iptvId, callback)</td>
    <td style="padding:15px">delete_iptv_channel</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/iptv_channels/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIptvChannel(account, iptvId, callback)</td>
    <td style="padding:15px">get_iptv_channel</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/iptv_channels/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIptvChannel(account, iptvId, body, callback)</td>
    <td style="padding:15px">update_iptv_channel</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/iptv_channels/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listMonitorTemplates(account, limit, offset, tag, tagName, callback)</td>
    <td style="padding:15px">list_monitor_templates</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/monitor_templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportMonitorTemplates(account, ids, callback)</td>
    <td style="padding:15px">export_monitor_templates</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/monitor_templates/export/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importMonitorTemplates(account, forceOverwrite, body, callback)</td>
    <td style="padding:15px">import_monitor_templates</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/monitor_templates/import/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitorTemplate(account, templateId, callback)</td>
    <td style="padding:15px">get_monitor_template</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/monitor_templates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMonitorTemplate(account, templateId, body, callback)</td>
    <td style="padding:15px">update_monitor_template</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/monitor_templates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listMonitors(account, limit, offset, start, end, basedOnTemplate, tag, tagName, callback)</td>
    <td style="padding:15px">list_monitors</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/monitors/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMonitor(account, body, callback)</td>
    <td style="padding:15px">create_monitor</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/monitors/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMonitor(account, monitorId, callback)</td>
    <td style="padding:15px">delete_monitor</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/monitors/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitor(account, monitorId, start, end, resolution, withDetailedMetrics, withMetricsAvg, withOtherResults, callback)</td>
    <td style="padding:15px">get_monitor</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/monitors/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchMonitor(account, monitorId, body, callback)</td>
    <td style="padding:15px">patch_monitor</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/monitors/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMonitor(account, monitorId, body, callback)</td>
    <td style="padding:15px">update_monitor</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/monitors/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitorPdfreport(account, monitorId, start, end, worstNum, graphs, callback)</td>
    <td style="padding:15px">get_monitor_pdfreport</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/monitors/{pathv2}/pdf_report?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNetworkDevices(account, limit, offset, tag, tagName, callback)</td>
    <td style="padding:15px">list_network_devices</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/network_devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkDevice(account, body, callback)</td>
    <td style="padding:15px">create_network_device</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/network_devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkDevice(account, networkDeviceId, callback)</td>
    <td style="padding:15px">delete_network_device</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/network_devices/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDevice(account, networkDeviceId, callback)</td>
    <td style="padding:15px">get_network_device</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/network_devices/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkDevicePatch(account, networkDeviceId, body, callback)</td>
    <td style="padding:15px">update_network_device_patch</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/network_devices/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkDevice(account, networkDeviceId, body, callback)</td>
    <td style="padding:15px">update_network_device</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/network_devices/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPingHosts(account, limit, offset, tag, tagName, callback)</td>
    <td style="padding:15px">list_ping_hosts</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/ping_hosts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPingHost(account, body, callback)</td>
    <td style="padding:15px">create_ping_host</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/ping_hosts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePingHost(account, pinghostId, callback)</td>
    <td style="padding:15px">delete_ping_host</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/ping_hosts/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPingHost(account, pinghostId, callback)</td>
    <td style="padding:15px">get_ping_host</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/ping_hosts/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePingHostPatch(account, pinghostId, body, callback)</td>
    <td style="padding:15px">update_ping_host_patch</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/ping_hosts/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePingHost(account, pinghostId, body, callback)</td>
    <td style="padding:15px">update_ping_host</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/ping_hosts/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSipAccounts(account, limit, offset, callback)</td>
    <td style="padding:15px">list_sip_accounts</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/sip_accounts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSipAccount(account, body, callback)</td>
    <td style="padding:15px">create_sip_account</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/sip_accounts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSipAccount(account, sipId, callback)</td>
    <td style="padding:15px">delete_sip_account</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/sip_accounts/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSipAccount(account, sipId, callback)</td>
    <td style="padding:15px">get_sip_account</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/sip_accounts/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSipAccount(account, sipId, body, callback)</td>
    <td style="padding:15px">update_sip_account</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/sip_accounts/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSnmpManagers(account, limit, offset, callback)</td>
    <td style="padding:15px">list_snmp_managers</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/snmp_managers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSnmpManager(account, body, callback)</td>
    <td style="padding:15px">create_snmp_manager</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/snmp_managers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSnmpManager(account, snmpManagerId, callback)</td>
    <td style="padding:15px">delete_snmp_manager</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/snmp_managers/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSnmpManager(account, snmpManagerId, callback)</td>
    <td style="padding:15px">get_snmp_manager</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/snmp_managers/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSnmpManager(account, snmpManagerId, body, callback)</td>
    <td style="padding:15px">update_snmp_manager</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/snmp_managers/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpeedtestResults(account, start, end, limit, offset, callback)</td>
    <td style="padding:15px">get_speedtest_results</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/speedtest/results/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpeedtest(account, speedtestId, callback)</td>
    <td style="padding:15px">get_speedtest</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/speedtest/results/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPublicSpeedtestcategories(account, limit, offset, callback)</td>
    <td style="padding:15px">list_public_speedtestcategories</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/speedtest_public/categories/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPublicSpeedtestinterfaces(account, limit, offset, callback)</td>
    <td style="padding:15px">list_public_speedtestinterfaces</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/speedtest_public/interfaces/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadPublicSpeedtestResult(account, body, callback)</td>
    <td style="padding:15px">upload_public_speedtest_result</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/speedtest_public/results/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPublicSpeedtests(account, speedtestToken, callback)</td>
    <td style="padding:15px">get_public_speedtests</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/speedtest_public/results/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSshKeys(account, testAgentId, limit, offset, callback)</td>
    <td style="padding:15px">list_ssh_keys</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/ssh_keys/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSshKey(account, testAgentId, body, callback)</td>
    <td style="padding:15px">create_ssh_key</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/ssh_keys/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSshKey(account, sshKeyId, testAgentId, callback)</td>
    <td style="padding:15px">delete_ssh_key</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/ssh_keys/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSshKey(account, sshKeyId, testAgentId, callback)</td>
    <td style="padding:15px">get_ssh_key</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/ssh_keys/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSshKey(account, sshKeyId, testAgentId, body, callback)</td>
    <td style="padding:15px">update_ssh_key</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/ssh_keys/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountStatistics(account, callback)</td>
    <td style="padding:15px">get_account_statistics</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/statistics/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstanceStatistics(callback)</td>
    <td style="padding:15px">get_instance_statistics</td>
    <td style="padding:15px">{base_path}/{version}/statistics/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTags(account, limit, offset, callback)</td>
    <td style="padding:15px">list_tags</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/tags/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTag(account, body, callback)</td>
    <td style="padding:15px">create_tag</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/tags/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTag(account, tagId, callback)</td>
    <td style="padding:15px">delete_tag</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/tags/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTag(account, tagId, callback)</td>
    <td style="padding:15px">get_tag</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/tags/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTag(account, tagId, body, callback)</td>
    <td style="padding:15px">update_tag</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/tags/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTestAgents(account, limit, offset, tag, tagName, callback)</td>
    <td style="padding:15px">list_test_agents</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_agents/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTestAgent(account, body, callback)</td>
    <td style="padding:15px">create_test_agent</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_agents/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rebootTestAgent(account, all, body, callback)</td>
    <td style="padding:15px">reboot_test_agent</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_agents/reboot/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">upgradeTestAgentSoftware(account, all, body, callback)</td>
    <td style="padding:15px">upgrade_test_agent_software</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_agents/update/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTestAgent(account, testAgentId, callback)</td>
    <td style="padding:15px">delete_test_agent</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_agents/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTestAgent(account, testAgentId, refreshSystemInformation, callback)</td>
    <td style="padding:15px">get_test_agent</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_agents/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTestAgent(account, testAgentId, body, callback)</td>
    <td style="padding:15px">patch_test_agent</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_agents/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTestAgent(account, testAgentId, body, callback)</td>
    <td style="padding:15px">update_test_agent</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_agents/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testAgentMoveCancel(account, testAgentId, callback)</td>
    <td style="padding:15px">test_agent_move_cancel</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_agents/{pathv2}/move/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testAgentMoveSchedule(account, testAgentId, body, callback)</td>
    <td style="padding:15px">test_agent_move_schedule</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_agents/{pathv2}/move/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scanWifiNetworksStop(account, testAgentNumericId, testAgentInterfaceName, callback)</td>
    <td style="padding:15px">scan_wifi_networks_stop</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_agents/{pathv2}/wifiscan/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scanWifiNetworksResults(account, testAgentNumericId, testAgentInterfaceName, callback)</td>
    <td style="padding:15px">scan_wifi_networks_results</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_agents/{pathv2}/wifiscan/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scanWifiNetworksStart(account, testAgentNumericId, testAgentInterfaceName, callback)</td>
    <td style="padding:15px">scan_wifi_networks_start</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_agents/{pathv2}/wifiscan/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTestTemplates(account, limit, offset, tag, tagName, callback)</td>
    <td style="padding:15px">list_test_templates</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportTestTemplates(account, ids, callback)</td>
    <td style="padding:15px">export_test_templates</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_templates/export/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importTestTemplates(account, forceOverwrite, body, callback)</td>
    <td style="padding:15px">import_test_templates</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_templates/import/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTestTemplate(account, templateId, callback)</td>
    <td style="padding:15px">get_test_template</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_templates/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTestTemplate(account, templateId, callback)</td>
    <td style="padding:15px">update_test_template</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/test_templates/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTests(account, limit, offset, tag, tagName, callback)</td>
    <td style="padding:15px">list_tests</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/tests/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTest(account, body, callback)</td>
    <td style="padding:15px">create_test</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/tests/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTest(account, testId, callback)</td>
    <td style="padding:15px">delete_test</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/tests/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTest(account, testId, withDetailedMetrics, withMetricsAvg, withOtherResults, callback)</td>
    <td style="padding:15px">get_test</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/tests/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTest(account, testId, body, callback)</td>
    <td style="padding:15px">update_test</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/tests/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTestPdfreport(account, testId, start, end, worstNum, graphs, callback)</td>
    <td style="padding:15px">get_test_pdfreport</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/tests/{pathv2}/pdf_report?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTwampReflectors(account, limit, offset, tag, tagName, callback)</td>
    <td style="padding:15px">list_twamp_reflectors</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/twamp_reflectors/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTwampReflector(account, body, callback)</td>
    <td style="padding:15px">create_twamp_reflector</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/twamp_reflectors/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTwampReflector(account, twampId, callback)</td>
    <td style="padding:15px">delete_twamp_reflector</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/twamp_reflectors/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTwampReflector(account, twampId, callback)</td>
    <td style="padding:15px">get_twamp_reflector</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/twamp_reflectors/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTwampReflectorPatch(account, twampId, body, callback)</td>
    <td style="padding:15px">update_twamp_reflector_patch</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/twamp_reflectors/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTwampReflector(account, twampId, body, callback)</td>
    <td style="padding:15px">update_twamp_reflector</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/twamp_reflectors/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listY1731Meps(account, limit, offset, tag, tagName, callback)</td>
    <td style="padding:15px">list_y1731_meps</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/y1731_meps/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createY1731Mep(account, body, callback)</td>
    <td style="padding:15px">create_y1731_mep</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/y1731_meps/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteY1731Mep(account, mepId, callback)</td>
    <td style="padding:15px">delete_y1731_mep</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/y1731_meps/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getY1731Mep(account, mepId, callback)</td>
    <td style="padding:15px">get_y1731_mep</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/y1731_meps/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateY1731Mep(account, mepId, body, callback)</td>
    <td style="padding:15px">update_y1731_mep</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/y1731_meps/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>

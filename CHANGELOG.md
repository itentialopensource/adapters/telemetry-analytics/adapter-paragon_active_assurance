
## 0.3.4 [10-14-2024]

* Changes made at 2024.10.14_19:38PM

See merge request itentialopensource/adapters/adapter-paragon_active_assurance!12

---

## 0.3.3 [09-14-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-paragon_active_assurance!10

---

## 0.3.2 [08-14-2024]

* Changes made at 2024.08.14_17:42PM

See merge request itentialopensource/adapters/adapter-paragon_active_assurance!9

---

## 0.3.1 [08-07-2024]

* Changes made at 2024.08.06_18:39PM

See merge request itentialopensource/adapters/adapter-paragon_active_assurance!8

---

## 0.3.0 [07-23-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-paragon_active_assurance!7

---

## 0.2.4 [03-27-2024]

* Changes made at 2024.03.27_13:07PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-paragon_active_assurance!6

---

## 0.2.3 [03-21-2024]

* Changes made at 2024.03.21_14:56PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-paragon_active_assurance!5

---

## 0.2.2 [03-12-2024]

* Changes made at 2024.03.12_10:55AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-paragon_active_assurance!4

---

## 0.2.1 [02-27-2024]

* Changes made at 2024.02.27_11:28AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-paragon_active_assurance!3

---

## 0.2.0 [01-04-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-paragon_active_assurance!2

---

## 0.1.2 [05-01-2023]

* Add multi step authentication

See merge request itentialopensource/adapters/telemetry-analytics/adapter-paragon_active_assurance!1

---

## 0.1.1 [03-13-2023]

* Bug fixes and performance improvements

See commit ca41529

---

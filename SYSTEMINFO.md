# Paragon Active Assurance

Vendor: Juniper Networks
Homepage: https://www.juniper.net/us/en.html

Product: Paragon Active Assurance (formerly Netrounds)
Product Page: https://www.juniper.net/us/en/products/network-automation/paragon-active-assurance.html

## Introduction
We classify Paragon Active Assurance into the Service Assurance domain as it is a solution that uses active testing and monitoring to ensure optimal performance and reliability.

## Why Integrate
The Paragon Active Assurance adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Paragon Active Assurance. With this adapter you have the ability to perform operations such as:

- Network Device
- Alarm
- Statistics

## Additional Product Documentation
The [API documents for Paragon Active Assurance](https://app.netrounds.com/rest/)

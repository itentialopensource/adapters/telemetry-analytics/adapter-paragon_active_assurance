# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Paragon_active_assurance System. The API that was used to build the adapter for Paragon_active_assurance is usually available in the report directory of this adapter. The adapter utilizes the Paragon_active_assurance API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Paragon Active Assurance adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Paragon Active Assurance. With this adapter you have the ability to perform operations such as:

- Network Device
- Alarm
- Statistics

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 

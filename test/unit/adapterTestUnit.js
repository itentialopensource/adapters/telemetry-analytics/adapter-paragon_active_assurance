/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-paragon_active_assurance',
      type: 'ParagonActiveAssurance',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const ParagonActiveAssurance = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Paragon_active_assurance Adapter Test', () => {
  describe('ParagonActiveAssurance Class Tests', () => {
    const a = new ParagonActiveAssurance(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('paragon_active_assurance'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('paragon_active_assurance'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('ParagonActiveAssurance', pronghornDotJson.export);
          assert.equal('Paragon_active_assurance', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-paragon_active_assurance', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('paragon_active_assurance'));
          assert.equal('ParagonActiveAssurance', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-paragon_active_assurance', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-paragon_active_assurance', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#listAlarmEmails - errors', () => {
      it('should have a listAlarmEmails function', (done) => {
        try {
          assert.equal(true, typeof a.listAlarmEmails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.listAlarmEmails(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-listAlarmEmails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAlarmEmails - errors', () => {
      it('should have a createAlarmEmails function', (done) => {
        try {
          assert.equal(true, typeof a.createAlarmEmails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.createAlarmEmails(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-createAlarmEmails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAlarmEmails - errors', () => {
      it('should have a deleteAlarmEmails function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAlarmEmails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.deleteAlarmEmails(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteAlarmEmails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing alarmEmailsId', (done) => {
        try {
          a.deleteAlarmEmails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'alarmEmailsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteAlarmEmails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmEmails - errors', () => {
      it('should have a getAlarmEmails function', (done) => {
        try {
          assert.equal(true, typeof a.getAlarmEmails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getAlarmEmails(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getAlarmEmails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing alarmEmailsId', (done) => {
        try {
          a.getAlarmEmails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'alarmEmailsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getAlarmEmails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAlarmEmails - errors', () => {
      it('should have a updateAlarmEmails function', (done) => {
        try {
          assert.equal(true, typeof a.updateAlarmEmails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.updateAlarmEmails(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateAlarmEmails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing alarmEmailsId', (done) => {
        try {
          a.updateAlarmEmails('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'alarmEmailsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateAlarmEmails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAlarmTemplates - errors', () => {
      it('should have a listAlarmTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.listAlarmTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.listAlarmTemplates(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-listAlarmTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAlarmTemplates - errors', () => {
      it('should have a createAlarmTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.createAlarmTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.createAlarmTemplates(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-createAlarmTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAlarmTemplate - errors', () => {
      it('should have a deleteAlarmTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAlarmTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.deleteAlarmTemplate(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteAlarmTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing alarmTemplateId', (done) => {
        try {
          a.deleteAlarmTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'alarmTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteAlarmTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmTemplate - errors', () => {
      it('should have a getAlarmTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.getAlarmTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getAlarmTemplate(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getAlarmTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing alarmTemplateId', (done) => {
        try {
          a.getAlarmTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'alarmTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getAlarmTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAlarmTemplate - errors', () => {
      it('should have a updateAlarmTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.updateAlarmTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.updateAlarmTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateAlarmTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing alarmTemplateId', (done) => {
        try {
          a.updateAlarmTemplate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'alarmTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateAlarmTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAlarms - errors', () => {
      it('should have a listAlarms function', (done) => {
        try {
          assert.equal(true, typeof a.listAlarms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.listAlarms(null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-listAlarms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAlarm - errors', () => {
      it('should have a deleteAlarm function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAlarm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.deleteAlarm(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteAlarm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing alarmId', (done) => {
        try {
          a.deleteAlarm('fakeparam', null, (data, error) => {
            try {
              const displayE = 'alarmId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteAlarm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarm - errors', () => {
      it('should have a getAlarm function', (done) => {
        try {
          assert.equal(true, typeof a.getAlarm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getAlarm(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getAlarm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing alarmId', (done) => {
        try {
          a.getAlarm('fakeparam', null, (data, error) => {
            try {
              const displayE = 'alarmId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getAlarm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAlarm - errors', () => {
      it('should have a updateAlarm function', (done) => {
        try {
          assert.equal(true, typeof a.updateAlarm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.updateAlarm(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateAlarm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing alarmId', (done) => {
        try {
          a.updateAlarm('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'alarmId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateAlarm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listExternalUrlShares - errors', () => {
      it('should have a listExternalUrlShares function', (done) => {
        try {
          assert.equal(true, typeof a.listExternalUrlShares === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.listExternalUrlShares(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-listExternalUrlShares', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createExternalUrlShare - errors', () => {
      it('should have a createExternalUrlShare function', (done) => {
        try {
          assert.equal(true, typeof a.createExternalUrlShare === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.createExternalUrlShare(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-createExternalUrlShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExternalUrlShare - errors', () => {
      it('should have a deleteExternalUrlShare function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExternalUrlShare === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.deleteExternalUrlShare(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteExternalUrlShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing shareId', (done) => {
        try {
          a.deleteExternalUrlShare('fakeparam', null, (data, error) => {
            try {
              const displayE = 'shareId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteExternalUrlShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExternalUrlShare - errors', () => {
      it('should have a getExternalUrlShare function', (done) => {
        try {
          assert.equal(true, typeof a.getExternalUrlShare === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getExternalUrlShare(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getExternalUrlShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing shareId', (done) => {
        try {
          a.getExternalUrlShare('fakeparam', null, (data, error) => {
            try {
              const displayE = 'shareId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getExternalUrlShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateExternalUrlShare - errors', () => {
      it('should have a updateExternalUrlShare function', (done) => {
        try {
          assert.equal(true, typeof a.updateExternalUrlShare === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.updateExternalUrlShare(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateExternalUrlShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing shareId', (done) => {
        try {
          a.updateExternalUrlShare('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'shareId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateExternalUrlShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listIpLookupTable - errors', () => {
      it('should have a listIpLookupTable function', (done) => {
        try {
          assert.equal(true, typeof a.listIpLookupTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.listIpLookupTable(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-listIpLookupTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIpLookupTable - errors', () => {
      it('should have a createIpLookupTable function', (done) => {
        try {
          assert.equal(true, typeof a.createIpLookupTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.createIpLookupTable(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-createIpLookupTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpLookupTable - errors', () => {
      it('should have a deleteIpLookupTable function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpLookupTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.deleteIpLookupTable(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteIpLookupTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipLookupTableId', (done) => {
        try {
          a.deleteIpLookupTable('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ipLookupTableId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteIpLookupTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpLookupTable - errors', () => {
      it('should have a getIpLookupTable function', (done) => {
        try {
          assert.equal(true, typeof a.getIpLookupTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getIpLookupTable(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getIpLookupTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipLookupTableId', (done) => {
        try {
          a.getIpLookupTable('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ipLookupTableId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getIpLookupTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIpLookupTable - errors', () => {
      it('should have a updateIpLookupTable function', (done) => {
        try {
          assert.equal(true, typeof a.updateIpLookupTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.updateIpLookupTable(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateIpLookupTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipLookupTableId', (done) => {
        try {
          a.updateIpLookupTable('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ipLookupTableId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateIpLookupTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listIptvChannels - errors', () => {
      it('should have a listIptvChannels function', (done) => {
        try {
          assert.equal(true, typeof a.listIptvChannels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.listIptvChannels(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-listIptvChannels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIptvChannel - errors', () => {
      it('should have a createIptvChannel function', (done) => {
        try {
          assert.equal(true, typeof a.createIptvChannel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.createIptvChannel(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-createIptvChannel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIptvChannel - errors', () => {
      it('should have a deleteIptvChannel function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIptvChannel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.deleteIptvChannel(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteIptvChannel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iptvId', (done) => {
        try {
          a.deleteIptvChannel('fakeparam', null, (data, error) => {
            try {
              const displayE = 'iptvId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteIptvChannel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIptvChannel - errors', () => {
      it('should have a getIptvChannel function', (done) => {
        try {
          assert.equal(true, typeof a.getIptvChannel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getIptvChannel(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getIptvChannel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iptvId', (done) => {
        try {
          a.getIptvChannel('fakeparam', null, (data, error) => {
            try {
              const displayE = 'iptvId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getIptvChannel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIptvChannel - errors', () => {
      it('should have a updateIptvChannel function', (done) => {
        try {
          assert.equal(true, typeof a.updateIptvChannel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.updateIptvChannel(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateIptvChannel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iptvId', (done) => {
        try {
          a.updateIptvChannel('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'iptvId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateIptvChannel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listMonitorTemplates - errors', () => {
      it('should have a listMonitorTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.listMonitorTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.listMonitorTemplates(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-listMonitorTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportMonitorTemplates - errors', () => {
      it('should have a exportMonitorTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.exportMonitorTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.exportMonitorTemplates(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-exportMonitorTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importMonitorTemplates - errors', () => {
      it('should have a importMonitorTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.importMonitorTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.importMonitorTemplates(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-importMonitorTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMonitorTemplate - errors', () => {
      it('should have a getMonitorTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.getMonitorTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getMonitorTemplate(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getMonitorTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getMonitorTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getMonitorTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMonitorTemplate - errors', () => {
      it('should have a updateMonitorTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.updateMonitorTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.updateMonitorTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateMonitorTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.updateMonitorTemplate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateMonitorTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listMonitors - errors', () => {
      it('should have a listMonitors function', (done) => {
        try {
          assert.equal(true, typeof a.listMonitors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.listMonitors(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-listMonitors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMonitor - errors', () => {
      it('should have a createMonitor function', (done) => {
        try {
          assert.equal(true, typeof a.createMonitor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.createMonitor(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-createMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMonitor - errors', () => {
      it('should have a deleteMonitor function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMonitor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.deleteMonitor(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing monitorId', (done) => {
        try {
          a.deleteMonitor('fakeparam', null, (data, error) => {
            try {
              const displayE = 'monitorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMonitor - errors', () => {
      it('should have a getMonitor function', (done) => {
        try {
          assert.equal(true, typeof a.getMonitor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getMonitor(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing monitorId', (done) => {
        try {
          a.getMonitor('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'monitorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchMonitor - errors', () => {
      it('should have a patchMonitor function', (done) => {
        try {
          assert.equal(true, typeof a.patchMonitor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.patchMonitor(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-patchMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing monitorId', (done) => {
        try {
          a.patchMonitor('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'monitorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-patchMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMonitor - errors', () => {
      it('should have a updateMonitor function', (done) => {
        try {
          assert.equal(true, typeof a.updateMonitor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.updateMonitor(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing monitorId', (done) => {
        try {
          a.updateMonitor('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'monitorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMonitorPdfreport - errors', () => {
      it('should have a getMonitorPdfreport function', (done) => {
        try {
          assert.equal(true, typeof a.getMonitorPdfreport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getMonitorPdfreport(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getMonitorPdfreport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing monitorId', (done) => {
        try {
          a.getMonitorPdfreport('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'monitorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getMonitorPdfreport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNetworkDevices - errors', () => {
      it('should have a listNetworkDevices function', (done) => {
        try {
          assert.equal(true, typeof a.listNetworkDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.listNetworkDevices(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-listNetworkDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkDevice - errors', () => {
      it('should have a createNetworkDevice function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.createNetworkDevice(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-createNetworkDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkDevice - errors', () => {
      it('should have a deleteNetworkDevice function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.deleteNetworkDevice(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteNetworkDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkDeviceId', (done) => {
        try {
          a.deleteNetworkDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkDeviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteNetworkDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDevice - errors', () => {
      it('should have a getNetworkDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getNetworkDevice(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getNetworkDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkDeviceId', (done) => {
        try {
          a.getNetworkDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkDeviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getNetworkDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkDevicePatch - errors', () => {
      it('should have a updateNetworkDevicePatch function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkDevicePatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.updateNetworkDevicePatch(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateNetworkDevicePatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkDeviceId', (done) => {
        try {
          a.updateNetworkDevicePatch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkDeviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateNetworkDevicePatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkDevice - errors', () => {
      it('should have a updateNetworkDevice function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.updateNetworkDevice(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateNetworkDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkDeviceId', (done) => {
        try {
          a.updateNetworkDevice('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkDeviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateNetworkDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPingHosts - errors', () => {
      it('should have a listPingHosts function', (done) => {
        try {
          assert.equal(true, typeof a.listPingHosts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.listPingHosts(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-listPingHosts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPingHost - errors', () => {
      it('should have a createPingHost function', (done) => {
        try {
          assert.equal(true, typeof a.createPingHost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.createPingHost(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-createPingHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePingHost - errors', () => {
      it('should have a deletePingHost function', (done) => {
        try {
          assert.equal(true, typeof a.deletePingHost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.deletePingHost(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deletePingHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pinghostId', (done) => {
        try {
          a.deletePingHost('fakeparam', null, (data, error) => {
            try {
              const displayE = 'pinghostId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deletePingHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPingHost - errors', () => {
      it('should have a getPingHost function', (done) => {
        try {
          assert.equal(true, typeof a.getPingHost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getPingHost(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getPingHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pinghostId', (done) => {
        try {
          a.getPingHost('fakeparam', null, (data, error) => {
            try {
              const displayE = 'pinghostId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getPingHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePingHostPatch - errors', () => {
      it('should have a updatePingHostPatch function', (done) => {
        try {
          assert.equal(true, typeof a.updatePingHostPatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.updatePingHostPatch(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updatePingHostPatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pinghostId', (done) => {
        try {
          a.updatePingHostPatch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pinghostId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updatePingHostPatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePingHost - errors', () => {
      it('should have a updatePingHost function', (done) => {
        try {
          assert.equal(true, typeof a.updatePingHost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.updatePingHost(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updatePingHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pinghostId', (done) => {
        try {
          a.updatePingHost('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pinghostId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updatePingHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSipAccounts - errors', () => {
      it('should have a listSipAccounts function', (done) => {
        try {
          assert.equal(true, typeof a.listSipAccounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.listSipAccounts(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-listSipAccounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSipAccount - errors', () => {
      it('should have a createSipAccount function', (done) => {
        try {
          assert.equal(true, typeof a.createSipAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.createSipAccount(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-createSipAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSipAccount - errors', () => {
      it('should have a deleteSipAccount function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSipAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.deleteSipAccount(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteSipAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sipId', (done) => {
        try {
          a.deleteSipAccount('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sipId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteSipAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSipAccount - errors', () => {
      it('should have a getSipAccount function', (done) => {
        try {
          assert.equal(true, typeof a.getSipAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getSipAccount(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getSipAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sipId', (done) => {
        try {
          a.getSipAccount('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sipId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getSipAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSipAccount - errors', () => {
      it('should have a updateSipAccount function', (done) => {
        try {
          assert.equal(true, typeof a.updateSipAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.updateSipAccount(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateSipAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sipId', (done) => {
        try {
          a.updateSipAccount('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sipId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateSipAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSnmpManagers - errors', () => {
      it('should have a listSnmpManagers function', (done) => {
        try {
          assert.equal(true, typeof a.listSnmpManagers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.listSnmpManagers(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-listSnmpManagers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSnmpManager - errors', () => {
      it('should have a createSnmpManager function', (done) => {
        try {
          assert.equal(true, typeof a.createSnmpManager === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.createSnmpManager(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-createSnmpManager', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSnmpManager - errors', () => {
      it('should have a deleteSnmpManager function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSnmpManager === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.deleteSnmpManager(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteSnmpManager', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snmpManagerId', (done) => {
        try {
          a.deleteSnmpManager('fakeparam', null, (data, error) => {
            try {
              const displayE = 'snmpManagerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteSnmpManager', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnmpManager - errors', () => {
      it('should have a getSnmpManager function', (done) => {
        try {
          assert.equal(true, typeof a.getSnmpManager === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getSnmpManager(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getSnmpManager', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snmpManagerId', (done) => {
        try {
          a.getSnmpManager('fakeparam', null, (data, error) => {
            try {
              const displayE = 'snmpManagerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getSnmpManager', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSnmpManager - errors', () => {
      it('should have a updateSnmpManager function', (done) => {
        try {
          assert.equal(true, typeof a.updateSnmpManager === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.updateSnmpManager(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateSnmpManager', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snmpManagerId', (done) => {
        try {
          a.updateSnmpManager('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'snmpManagerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateSnmpManager', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSpeedtestResults - errors', () => {
      it('should have a getSpeedtestResults function', (done) => {
        try {
          assert.equal(true, typeof a.getSpeedtestResults === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getSpeedtestResults(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getSpeedtestResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSpeedtest - errors', () => {
      it('should have a getSpeedtest function', (done) => {
        try {
          assert.equal(true, typeof a.getSpeedtest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getSpeedtest(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getSpeedtest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing speedtestId', (done) => {
        try {
          a.getSpeedtest('fakeparam', null, (data, error) => {
            try {
              const displayE = 'speedtestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getSpeedtest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPublicSpeedtestcategories - errors', () => {
      it('should have a listPublicSpeedtestcategories function', (done) => {
        try {
          assert.equal(true, typeof a.listPublicSpeedtestcategories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.listPublicSpeedtestcategories(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-listPublicSpeedtestcategories', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPublicSpeedtestinterfaces - errors', () => {
      it('should have a listPublicSpeedtestinterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.listPublicSpeedtestinterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.listPublicSpeedtestinterfaces(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-listPublicSpeedtestinterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadPublicSpeedtestResult - errors', () => {
      it('should have a uploadPublicSpeedtestResult function', (done) => {
        try {
          assert.equal(true, typeof a.uploadPublicSpeedtestResult === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.uploadPublicSpeedtestResult(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-uploadPublicSpeedtestResult', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPublicSpeedtests - errors', () => {
      it('should have a getPublicSpeedtests function', (done) => {
        try {
          assert.equal(true, typeof a.getPublicSpeedtests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getPublicSpeedtests(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getPublicSpeedtests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing speedtestToken', (done) => {
        try {
          a.getPublicSpeedtests('fakeparam', null, (data, error) => {
            try {
              const displayE = 'speedtestToken is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getPublicSpeedtests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSshKeys - errors', () => {
      it('should have a listSshKeys function', (done) => {
        try {
          assert.equal(true, typeof a.listSshKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.listSshKeys(null, null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-listSshKeys', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testAgentId', (done) => {
        try {
          a.listSshKeys('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'testAgentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-listSshKeys', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSshKey - errors', () => {
      it('should have a createSshKey function', (done) => {
        try {
          assert.equal(true, typeof a.createSshKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.createSshKey(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-createSshKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testAgentId', (done) => {
        try {
          a.createSshKey('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'testAgentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-createSshKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSshKey - errors', () => {
      it('should have a deleteSshKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSshKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.deleteSshKey(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteSshKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sshKeyId', (done) => {
        try {
          a.deleteSshKey('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sshKeyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteSshKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testAgentId', (done) => {
        try {
          a.deleteSshKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'testAgentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteSshKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSshKey - errors', () => {
      it('should have a getSshKey function', (done) => {
        try {
          assert.equal(true, typeof a.getSshKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getSshKey(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getSshKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sshKeyId', (done) => {
        try {
          a.getSshKey('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sshKeyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getSshKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testAgentId', (done) => {
        try {
          a.getSshKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'testAgentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getSshKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSshKey - errors', () => {
      it('should have a updateSshKey function', (done) => {
        try {
          assert.equal(true, typeof a.updateSshKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.updateSshKey(null, null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateSshKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sshKeyId', (done) => {
        try {
          a.updateSshKey('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'sshKeyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateSshKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testAgentId', (done) => {
        try {
          a.updateSshKey('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'testAgentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateSshKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountStatistics - errors', () => {
      it('should have a getAccountStatistics function', (done) => {
        try {
          assert.equal(true, typeof a.getAccountStatistics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getAccountStatistics(null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getAccountStatistics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInstanceStatistics - errors', () => {
      it('should have a getInstanceStatistics function', (done) => {
        try {
          assert.equal(true, typeof a.getInstanceStatistics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTags - errors', () => {
      it('should have a listTags function', (done) => {
        try {
          assert.equal(true, typeof a.listTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.listTags(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-listTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTag - errors', () => {
      it('should have a createTag function', (done) => {
        try {
          assert.equal(true, typeof a.createTag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.createTag(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-createTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTag - errors', () => {
      it('should have a deleteTag function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.deleteTag(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tagId', (done) => {
        try {
          a.deleteTag('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tagId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTag - errors', () => {
      it('should have a getTag function', (done) => {
        try {
          assert.equal(true, typeof a.getTag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getTag(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tagId', (done) => {
        try {
          a.getTag('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tagId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTag - errors', () => {
      it('should have a updateTag function', (done) => {
        try {
          assert.equal(true, typeof a.updateTag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.updateTag(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tagId', (done) => {
        try {
          a.updateTag('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'tagId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTestAgents - errors', () => {
      it('should have a listTestAgents function', (done) => {
        try {
          assert.equal(true, typeof a.listTestAgents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.listTestAgents(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-listTestAgents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTestAgent - errors', () => {
      it('should have a createTestAgent function', (done) => {
        try {
          assert.equal(true, typeof a.createTestAgent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.createTestAgent(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-createTestAgent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rebootTestAgent - errors', () => {
      it('should have a rebootTestAgent function', (done) => {
        try {
          assert.equal(true, typeof a.rebootTestAgent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.rebootTestAgent(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-rebootTestAgent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#upgradeTestAgentSoftware - errors', () => {
      it('should have a upgradeTestAgentSoftware function', (done) => {
        try {
          assert.equal(true, typeof a.upgradeTestAgentSoftware === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.upgradeTestAgentSoftware(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-upgradeTestAgentSoftware', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTestAgent - errors', () => {
      it('should have a deleteTestAgent function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTestAgent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.deleteTestAgent(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteTestAgent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testAgentId', (done) => {
        try {
          a.deleteTestAgent('fakeparam', null, (data, error) => {
            try {
              const displayE = 'testAgentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteTestAgent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTestAgent - errors', () => {
      it('should have a getTestAgent function', (done) => {
        try {
          assert.equal(true, typeof a.getTestAgent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getTestAgent(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getTestAgent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testAgentId', (done) => {
        try {
          a.getTestAgent('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'testAgentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getTestAgent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTestAgent - errors', () => {
      it('should have a patchTestAgent function', (done) => {
        try {
          assert.equal(true, typeof a.patchTestAgent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.patchTestAgent(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-patchTestAgent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testAgentId', (done) => {
        try {
          a.patchTestAgent('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'testAgentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-patchTestAgent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTestAgent - errors', () => {
      it('should have a updateTestAgent function', (done) => {
        try {
          assert.equal(true, typeof a.updateTestAgent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.updateTestAgent(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateTestAgent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testAgentId', (done) => {
        try {
          a.updateTestAgent('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'testAgentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateTestAgent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#testAgentMoveCancel - errors', () => {
      it('should have a testAgentMoveCancel function', (done) => {
        try {
          assert.equal(true, typeof a.testAgentMoveCancel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.testAgentMoveCancel(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-testAgentMoveCancel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testAgentId', (done) => {
        try {
          a.testAgentMoveCancel('fakeparam', null, (data, error) => {
            try {
              const displayE = 'testAgentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-testAgentMoveCancel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#testAgentMoveSchedule - errors', () => {
      it('should have a testAgentMoveSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.testAgentMoveSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.testAgentMoveSchedule(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-testAgentMoveSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testAgentId', (done) => {
        try {
          a.testAgentMoveSchedule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'testAgentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-testAgentMoveSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scanWifiNetworksStop - errors', () => {
      it('should have a scanWifiNetworksStop function', (done) => {
        try {
          assert.equal(true, typeof a.scanWifiNetworksStop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.scanWifiNetworksStop(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-scanWifiNetworksStop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testAgentNumericId', (done) => {
        try {
          a.scanWifiNetworksStop('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'testAgentNumericId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-scanWifiNetworksStop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testAgentInterfaceName', (done) => {
        try {
          a.scanWifiNetworksStop('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'testAgentInterfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-scanWifiNetworksStop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scanWifiNetworksResults - errors', () => {
      it('should have a scanWifiNetworksResults function', (done) => {
        try {
          assert.equal(true, typeof a.scanWifiNetworksResults === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.scanWifiNetworksResults(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-scanWifiNetworksResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testAgentNumericId', (done) => {
        try {
          a.scanWifiNetworksResults('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'testAgentNumericId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-scanWifiNetworksResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testAgentInterfaceName', (done) => {
        try {
          a.scanWifiNetworksResults('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'testAgentInterfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-scanWifiNetworksResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scanWifiNetworksStart - errors', () => {
      it('should have a scanWifiNetworksStart function', (done) => {
        try {
          assert.equal(true, typeof a.scanWifiNetworksStart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.scanWifiNetworksStart(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-scanWifiNetworksStart', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testAgentNumericId', (done) => {
        try {
          a.scanWifiNetworksStart('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'testAgentNumericId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-scanWifiNetworksStart', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testAgentInterfaceName', (done) => {
        try {
          a.scanWifiNetworksStart('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'testAgentInterfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-scanWifiNetworksStart', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTestTemplates - errors', () => {
      it('should have a listTestTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.listTestTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.listTestTemplates(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-listTestTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportTestTemplates - errors', () => {
      it('should have a exportTestTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.exportTestTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.exportTestTemplates(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-exportTestTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importTestTemplates - errors', () => {
      it('should have a importTestTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.importTestTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.importTestTemplates(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-importTestTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTestTemplate - errors', () => {
      it('should have a getTestTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.getTestTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getTestTemplate(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getTestTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getTestTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getTestTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTestTemplate - errors', () => {
      it('should have a updateTestTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.updateTestTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.updateTestTemplate(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateTestTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.updateTestTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateTestTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTests - errors', () => {
      it('should have a listTests function', (done) => {
        try {
          assert.equal(true, typeof a.listTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.listTests(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-listTests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTest - errors', () => {
      it('should have a createTest function', (done) => {
        try {
          assert.equal(true, typeof a.createTest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.createTest(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-createTest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTest - errors', () => {
      it('should have a deleteTest function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.deleteTest(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteTest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testId', (done) => {
        try {
          a.deleteTest('fakeparam', null, (data, error) => {
            try {
              const displayE = 'testId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteTest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTest - errors', () => {
      it('should have a getTest function', (done) => {
        try {
          assert.equal(true, typeof a.getTest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getTest(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getTest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testId', (done) => {
        try {
          a.getTest('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'testId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getTest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTest - errors', () => {
      it('should have a updateTest function', (done) => {
        try {
          assert.equal(true, typeof a.updateTest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.updateTest(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateTest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testId', (done) => {
        try {
          a.updateTest('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'testId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateTest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTestPdfreport - errors', () => {
      it('should have a getTestPdfreport function', (done) => {
        try {
          assert.equal(true, typeof a.getTestPdfreport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getTestPdfreport(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getTestPdfreport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing testId', (done) => {
        try {
          a.getTestPdfreport('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'testId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getTestPdfreport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTwampReflectors - errors', () => {
      it('should have a listTwampReflectors function', (done) => {
        try {
          assert.equal(true, typeof a.listTwampReflectors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.listTwampReflectors(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-listTwampReflectors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTwampReflector - errors', () => {
      it('should have a createTwampReflector function', (done) => {
        try {
          assert.equal(true, typeof a.createTwampReflector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.createTwampReflector(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-createTwampReflector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTwampReflector - errors', () => {
      it('should have a deleteTwampReflector function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTwampReflector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.deleteTwampReflector(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteTwampReflector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing twampId', (done) => {
        try {
          a.deleteTwampReflector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'twampId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteTwampReflector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTwampReflector - errors', () => {
      it('should have a getTwampReflector function', (done) => {
        try {
          assert.equal(true, typeof a.getTwampReflector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getTwampReflector(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getTwampReflector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing twampId', (done) => {
        try {
          a.getTwampReflector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'twampId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getTwampReflector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTwampReflectorPatch - errors', () => {
      it('should have a updateTwampReflectorPatch function', (done) => {
        try {
          assert.equal(true, typeof a.updateTwampReflectorPatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.updateTwampReflectorPatch(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateTwampReflectorPatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing twampId', (done) => {
        try {
          a.updateTwampReflectorPatch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'twampId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateTwampReflectorPatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTwampReflector - errors', () => {
      it('should have a updateTwampReflector function', (done) => {
        try {
          assert.equal(true, typeof a.updateTwampReflector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.updateTwampReflector(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateTwampReflector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing twampId', (done) => {
        try {
          a.updateTwampReflector('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'twampId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateTwampReflector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listY1731Meps - errors', () => {
      it('should have a listY1731Meps function', (done) => {
        try {
          assert.equal(true, typeof a.listY1731Meps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.listY1731Meps(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-listY1731Meps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createY1731Mep - errors', () => {
      it('should have a createY1731Mep function', (done) => {
        try {
          assert.equal(true, typeof a.createY1731Mep === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.createY1731Mep(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-createY1731Mep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteY1731Mep - errors', () => {
      it('should have a deleteY1731Mep function', (done) => {
        try {
          assert.equal(true, typeof a.deleteY1731Mep === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.deleteY1731Mep(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteY1731Mep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mepId', (done) => {
        try {
          a.deleteY1731Mep('fakeparam', null, (data, error) => {
            try {
              const displayE = 'mepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-deleteY1731Mep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getY1731Mep - errors', () => {
      it('should have a getY1731Mep function', (done) => {
        try {
          assert.equal(true, typeof a.getY1731Mep === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.getY1731Mep(null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getY1731Mep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mepId', (done) => {
        try {
          a.getY1731Mep('fakeparam', null, (data, error) => {
            try {
              const displayE = 'mepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-getY1731Mep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateY1731Mep - errors', () => {
      it('should have a updateY1731Mep function', (done) => {
        try {
          assert.equal(true, typeof a.updateY1731Mep === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing account', (done) => {
        try {
          a.updateY1731Mep(null, null, null, (data, error) => {
            try {
              const displayE = 'account is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateY1731Mep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mepId', (done) => {
        try {
          a.updateY1731Mep('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'mepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_active_assurance-adapter-updateY1731Mep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
